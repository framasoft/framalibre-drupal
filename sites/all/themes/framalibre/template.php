<?php
/**
 * @file
 * template.php
 */


//----------------------------------------------------------------------
//-- ALTERATIONS
//----------------------------------------------------------------------


/**
 * Implements hook_theme_registry_alter().
 */
function framalibre_theme_registry_alter(&$theme_registry) {
  $theme_registry['links']['variables']['list_group'] = FALSE;
}


/**
 * Implements hook_node_view_alter(&$build).
 */
function framalibre_node_view_alter(&$build) {
  if (!empty($build['links']['node']['#links']['faq_back_to_top'])) {
    $build['links']['node']['#links']['faq_back_to_top']['title'] .= '&nbsp;<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>';
  }
}


/**
 * Implements hook_preprocess_html().
 * @see html.tpl.php
 */
function framalibre_preprocess_html(&$vars) {
  // Includes Bootstrap stylesheets
  if (theme_get_setting('framalibre_use_framanav_bootstrap_styles')) {
    drupal_add_css(path_to_theme() . '/framanav/lib/bootstrap/css/bootstrap.min.css');
  } else {
    drupal_add_css(path_to_theme() . '/css/bootstrap.css');
  }

  // Includes Framasoft stylesheets
  drupal_add_css(path_to_theme() . '/framanav/css/frama.css');

  // Includes Framalibre stylesheets
  drupal_add_css(path_to_theme() . '/css/style.css');

  // Includes Bootstrap scripts
  if (theme_get_setting('framalibre_use_framanav_bootstrap_scripts')) {
    drupal_add_js(path_to_theme() . '/framanav/lib/bootstrap/js/bootstrap.min.js', array('group' => JS_LIBRARY));
  }
  else {
    drupal_add_js(path_to_theme() . '/bootstrap/js/affix.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/alert.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/button.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/carousel.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/collapse.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/dropdown.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/modal.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/tooltip.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/popover.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/scrollspy.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/tab.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/bootstrap/js/transition.js', array('group' => JS_LIBRARY));
  }

  // Includes JS script of the Framasoft navigation bar
  drupal_add_js('
    var script = document.createElement(\'script\');
    script.type = "text/javascript";
    script.src = "https://framasoft.org/nav/nav.js";
    document.getElementsByTagName(\'head\')[0].appendChild(script);',
    array('type' => 'inline')
  );

  // Includes JS script for the login form popover
  if (!user_is_logged_in()) {
    drupal_add_js(path_to_theme() . '/js/login-popover.js');
  }

  // Determines if the sidebar must be placed at the right of the main content.
  if ((!empty($vars['page']['sidebar_first']) || !empty($vars['page']['sidebar_second']))
    && drupal_match_path(current_path(), theme_get_setting('framalibre_sidebar_on_right'))) {
    $vars['classes_array'][] = 'sidebar-right';
  } else {
    $vars['classes_array'][] = 'sidebar-left';
  }
}


/**
 * Implements hook_preprocess_page().
 * @see page.tpl.php
 */
function framalibre_preprocess_page(&$vars) {
  // Do not display the page title in page.tpl.php if we're viewing a node,
  // a user, a taxonomy term or the front page.
  if (!isset($vars['display_title'])) {
    if (drupal_is_front_page()) {
      $vars['display_title'] = FALSE;
    }
    elseif (isset($vars['node'])) {
      $vars['display_title'] = FALSE;
    }
    else {
      $menu_item = menu_get_item();
      if ($menu_item['path'] == 'user/%' || $menu_item['path'] == 'taxonomy/term/%') {
        $vars['display_title'] = FALSE;
      }
    }
  }

  $vars['display_title'] = isset($vars['display_title']) ? $vars['display_title'] : TRUE;
}


/**
 * Implements hook_preprocess_region().
 * @see region.tpl.php
 */
function framalibre_preprocess_region(&$vars) {
  if ($vars['region'] == 'sidebar_first') {
    //$vars['alternatives_page_url'] = url(variable_get('alternatives_page_path', 'alternative-free-softwares'));
    $vars['alternatives_page_url'] = url(variable_get('alternatives_page_path', 'alternatives'));
  }
}


/**
 * Implements hook_preprocess_node().
 * @see node.tpl.php
 */
function framalibre_preprocess_node(&$vars) {
  $node = $vars['node'];
  $suggestions = &$vars['theme_hook_suggestions'];

  // Adds a view mode specific CSS class on the node
  $vars['classes_array'][] = 'node-' . $vars['view_mode'];

  // Adds a CSS class on the node title
  $vars['title_attributes_array']['class'][] = 'node-title';

  // Adds new theme hook suggestions
  array_unshift($suggestions, 'node__' . $node->type . '__' . $vars['view_mode']);

  if (module_exists('framastuff') && in_array($node->type, _framastuff_get_sheet_content_types())) {
    // Adds new theme hook suggestions for the sheet nodes
    $backup = array_splice($suggestions, 0, 2);

    array_unshift(
      $suggestions,
      $backup[0],
      $backup[1],
      'node__sheet__' . $vars['view_mode'],
      'node__sheet'
    );

    // Adds a CSS class for the sheet nodes
    $vars['classes_array'][] = 'node-sheet';
    $vars['submitted'] = t('By !username', array('!username' => $vars['name']));
  }
}


/**
 * Implements hook_preprocess_comment().
 * @see comment.tpl.php
 */
function framalibre_preprocess_comment(&$vars) {
  // Adds bootstrap CSS class on comment title
  $vars['title_attributes_array']['class'][] = 'media-heading';

  // Alters submitter information
  //$vars['created'] = format_date($comment->created, 'comment_date');
  $vars['submitted'] = t('!username on !datetime', array('!username' => $vars['author'], '!datetime' => $vars['created']));
}


/**
 * Implements hook_preprocess_field().
 * @see field.tpl.php
 */
function framalibre_preprocess_field(&$vars) {
  $element = $vars['element'];

  if ($element['#field_name'] == 'field_capture_d_cran' && !isset($vars['content_attributes_array']['id'])) {
    $vars['content_attributes_array']['id'] = drupal_html_id($vars['field_name_css'] . '-carousel');
  }

  // Displays categories as label
  if ($element['#field_name'] == 'field_annuaires') {
    foreach ($vars['items'] as &$item) {
      if (!empty($item['#type']) && $item['#type'] == 'link') {
        $item['#options']['attributes']['class'][] = 'label label-primary';
      }
    }
  }

  // Adds media-object class
  if (($element['#field_name'] == 'field_logo' && in_array($element['#view_mode'], array('full', 'teaser', 'alternative')))
    || ($element['#field_name'] == 'field_image' && $element['#view_mode'] == 'teaser')) {
    foreach ($vars['items'] as &$item) {
      $item['#item']['attributes']['class'][] = 'media-object';
    }
  }

  // Official link displayed as globe
  if ($element['#field_name'] == 'field_lien_officiel' && $element['#view_mode'] == 'teaser') {
    foreach ($vars['items'] as &$item) {
      $item['#element']['title'] = '<span class="glyphicon glyphicon-globe"></span>'
        . '<span class="sr-only">' . $item['#element']['title'] . '</span>';
      $item['#element']['attributes']['title'] = t('View the official site');
    }
  }
}


/**
 * Implements hook_preprocess_block().
 * @see block.tpl.php
 */
function framalibre_preprocess_block(&$vars) {
  $block = $vars['block'];

  // Collapsible blocks
  $collapsible = in_array($block->delta, array('search', 'user-menu'))
    || ($block->module == 'taxonomy_menu_block' && in_array($block->region, array('navigation', 'sidebar_first')));

  if ($collapsible) {
    $vars['classes_array'][] = 'collapse';
  }
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_taxonomy_term(&$vars) {
  // Adds a new template suggestion
  $suggestion = 'taxonomy_term__' . $vars['vocabulary_machine_name'] . '__' . $vars['view_mode'];
  array_unshift($vars['theme_hook_suggestions'], $suggestion);

  // Adds CSS class
  $vars['classes_array'][] = 'taxonomy-term-' . $vars['view_mode'];
  $vars['title_classes_array'][] = 'taxonomy-term-' . $vars['view_mode'];
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_user_profile(&$vars) {
  $account = $vars['elements']['#account'];

  $website = field_get_items('user', $account, 'field_site_web');
  if ($website) {
    $vars['website_url'] = $website[0]['value'];
  }

  $twitter = field_get_items('user', $account, 'field_twitter');
  if ($twitter) {
    $vars['twitter_name'] = $twitter[0]['value'];
    $vars['twitter_url'] = 'https://www.twitter.com/' . str_replace('@', '', $twitter[0]['value']);
  }
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_link(&$vars) {
  if ($vars['path'] == 'user/login') {
    $vars['options']['attributes']['class'][] = 'login';
  }
}


/**
 * Overrides theme_links().
 */
function framalibre_links(&$vars) {
  if ($vars['list_group']) {
    $links = $vars['links'];
    $attributes = $vars['attributes'];
    //$heading = $vars['heading'];

    global $language_url;
    $output = '';

    if (count($links) > 0) {
      $attributes['class'][] = 'list-group';

      $output .= '<div' . drupal_attributes($attributes) . '>';

      $num_links = count($links);
      $i = 1;

      foreach ($links as $key => $link) {
        $class = array('list-group-item', $key);

        // Add first, last and active classes to the list of links to help out themers.
        if ($i == 1) {
          $class[] = 'first';
        }
        if ($i == $num_links) {
          $class[] = 'last';
        }
        if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language_url->language)) {
          $class[] = 'active';
        }

        if (!isset($link['attributes']['class'])) {
          $link['attributes']['class'] = array();
        }

        $link['attributes']['class'] += $class;

        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);

        $i++;
      }

      $output .= '</div>';
    }

    return $output;
  }
  else {
    return theme_links($vars);
  }
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_taxonomy_menu_block(&$vars) {
  //$vocabulary = taxonomy_vocabulary_load($vars['config']['vid']);
  //$vars['theme_hook_suggestions'][] = 'taxonomy_menu_block__' . $vocabulary->machine_name;
  $block = _framalibre_block_load('taxonomy_menu_block', $vars['config']['delta']);
  $vars['theme_hook_suggestions'][] = 'taxonomy_menu_block__' . $block->region;
}


/**
 * Overrides theme_taxonomy_menu_block() for the navigation region.
 */
function framalibre_taxonomy_menu_block__navigation($vars) {
  return theme('menu_navigation', array(
    'items' => $vars['items'],
    'config' => $vars['config'],
  ));
}


/**
 * Overrides theme_taxonomy_menu_block() for the primary sidebar region.
 */
function framalibre_taxonomy_menu_block__sidebar_first($vars) {
  return theme('menu_sidebar',  array(
    'items' => $vars['items'],
    'config' => $vars['config'],
  ));
}


/**
 * Overrides theme_taxonomy_menu_block() for the second sidebar region.
 */
function framalibre_taxonomy_menu_block__sidebar_second($vars) {
  return theme('menu_sidebar',  array(
    'items' => $vars['items'],
    'config' => $vars['config'],
  ));
}


/**
 * Overrides theme_pager().
 */
function framalibre_pager($vars) {
  // Removes the text center div added by bootstrap...
  $bootstrap_pager = bootstrap_pager($vars);
  return substr($bootstrap_pager, 25, count($bootstrap_pager) - 7);
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_views_view(&$vars) {
  // Adds CSS class based on the style plugin name
  $vars['classes_array'][] = 'view-style-' . $vars['view']->style_plugin->plugin_name;

//  if ($vars['view']->name == 'recherche_par_criteres') {
//    if (empty($vars['view']->exposed_input)) {
//      $vars['rows']  = '';
//      $vars['pager'] = '';
//      $vars['empty'] = '';
//    }
//  }
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_views_view_unformatted(&$vars) {
  if ($vars['view']->name == 'recherche_par_criteres') {
    $vars['groups'] = array();
    foreach ($vars['view']->result as $index => $item) {
      $vars['groups'][$item->node_type][] = $vars['rows'][$index];
    }
  }
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_views_exposed_form(&$vars) {
  if ($vars['form']['#id'] == 'views-exposed-form-recherche-par-criteres-page') {
    foreach ($vars['widgets'] as $key => $widget) {
      if (empty($widget->widget)) {
        unset($vars['widgets'][$key]);
      }
    }

    if (!empty($vars['sort_by'])) {
      $vars['widgets']['sort'] = $vars['sort_by'] . $vars['sort_order'];
    }

    if (!empty($vars['items_per_page'])) {
      $vars['widgets']['items-per-page'] = $vars['items_per_page'];
    }

    if (!empty($vars['offset'])) {
      $vars['widgets']['offset'] = $vars['offset'];
    }

    if (!empty($vars['button'])) {
      $vars['widgets']['button'] = $vars['button'];
      if (!empty($vars['reset_button'])) {
        $vars['widgets']['button'] .= $vars['reset_button'];
      }
    }

    // Adds a submit button to display near the keys filter field
    $submit = $vars['form']['submit'];
    $submit['#access'] = TRUE;
    $submit['#id'] = str_replace('submit', 'keys-submit', $submit['#id']);
    $vars['keys_submit'] = drupal_render($submit);

    // Determines if the fieldset must be collapsed or not
    $params = $_GET;
    unset($params['q']);
    unset($params['keys']);
    unset($params['page']);
    $vars['collapsed'] = !((boolean) count($params));
  }
}


//----------------------------------------------------------------------
//-- THEMES
//----------------------------------------------------------------------


/**
 * Implements hook_theme().
 */
function framalibre_theme($existing, $type, $theme, $path) {
  $themes = array();

  $themes['button_dropdown'] = array(
    'variables' => array(
      'label' => NULL,
      'links' => array(),
      'attributes' => array(),
      'type' => 'default',
      'vertical_align' => 'left',
    ),
    'template' => 'button-dropdown',
    'path' => $path . '/templates',
  );

  $themes['menu_navigation'] = array(
    'variables' => array(
      'items' => array(),
      'config' => NULL,
    ),
    'template' => 'menu-navigation',
    'path' => $path . '/templates/menu',
  );

  $themes['menu_sidebar'] = array(
    'variables' => array(
      'items' => array(),
      'config' => NULL,
    ),
    'template' => 'menu-sidebar',
    'path' => $path . '/templates/menu',
  );

  return $themes;
}


/**
 * Implements hook_preprocess_THEME().
 */
function framalibre_preprocess_button_dropdown(&$vars) {
  // Adds button dropdown class
  $vars['attributes']['class'][] = 'btn-group';

  // Generates dropdown menu
  $vars['dropdown_menu'] = '';

  $menu_classes = array('dropdown-menu');
  if (isset($vars['vertical_align']) && $vars['vertical_align'] == 'right') {
    $menu_classes[] = 'dropdown-menu-right';
  }

  if (!empty($vars['links'])) {
    $links_vars = array(
      'links' => $vars['links'],
      'heading' => NULL,
      'attributes' => array(
        'class' => $menu_classes,
      ),
    );

    $vars['dropdown_menu'] = theme_links($links_vars);
  }
}


//----------------------------------------------------------------------
//-- HELPERS
//----------------------------------------------------------------------


function _framalibre_block_load($module, $delta) {
  global $theme;

  $sql = 'SELECT * FROM {block} WHERE module = :module AND delta = :delta AND theme = :theme';

  $query = db_query($sql, array(
    ':module' => $module,
    ':delta' => $delta,
    ':theme' => $theme,
  ));

  return $query->fetchObject();
}
