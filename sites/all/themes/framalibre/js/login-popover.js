(function ($) {

  Drupal.behaviors.loginPopover = {
    attach: function (context, settings) {
      var $loginLinks = $('a.login', context);

      if ($loginLinks.length) {
        var $loginPopover = $('#block-user-login', context).detach();

        if ($loginPopover.length) {
          var title = $loginPopover.find('.block-title');
          title = (title.length) ? title.detach().text() : '';

          var content = $loginPopover.html();

          $loginLinks
            .attr('href', null)
            .popover({
              html: true,
              title: title,
              content: content,
              placement: 'auto bottom',
              container: '.main-container',
              viewport: {
                selector: '.main-container',
                padding: 30
              }
            });

          $loginLinks.on('inserted.bs.popover', function(event) {
            Drupal.attachBehaviors($('#' + $(this).attr('aria-describedby')), settings);
          });
        }
      }
    }
  };

})(jQuery);