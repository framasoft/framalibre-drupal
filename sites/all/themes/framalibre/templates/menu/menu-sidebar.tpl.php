<nav>
  <ul class="list-group">
    <?php foreach ($items as $index => $item): ?>
    <?php
      $has_children = !empty($item['children']);
      $is_active = ($item['active_trail'] == TRUE);
    ?>
    <li class="list-group-item<?php print ($is_active) ? ' active' : ''; ?>">
      <a href="<?php print url($item['path']); ?>" class="">
        <?php print $item['name']; ?>
        <?php if ($is_active): ?>
        <span class="sr-only"><?php print t('(current)'); ?></span>
        <?php endif; ?>
      </a>

      <?php if ($has_children): ?>
      <?php $sub_menu_id = 'sub-menu-' . $config['delta'] . $index; ?>
      <a href="#<?php print $sub_menu_id; ?>"
        data-toggle="collapse"
        aria-expanded="<?php print ($is_active) ? 'true' : 'false'; ?>"
        aria-controls="<?php print $sub_menu_id; ?>">
      </a>
      <?php endif; ?>

      <?php if ($has_children): ?>
      <div id="<?php print $sub_menu_id; ?>" class="sub-menu collapse<?php print ($is_active) ? ' in' : ''; ?>">
        <?php foreach ($item['children'] as $child): ?>
        <a href="<?php print url($child['path']); ?>" class="<?php print ($child['active_trail'] == TRUE) ? ' active' : ''; ?>">
          <?php print $child['name']; ?>
        </a>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>
    </li>
    <?php endforeach; ?>
  </ul>
</nav>