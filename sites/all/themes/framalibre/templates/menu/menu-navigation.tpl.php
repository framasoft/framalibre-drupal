<nav class="navbar navbar-default navbar-collapse" id="navbar-<?php print $config['delta']; ?>">
  <ul class="nav navbar-nav">
    <?php foreach ($items as $item): ?>
    <?php
      $has_children = !empty($item['children']);
      $is_active = ($item['active_trail'] == TRUE);
    ?>
    <li<?php if ($is_active): ?> class="active"<?php endif; ?>>
      <a href="<?php print url($item['path']); ?>"<?php if ($has_children): ?> class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"<?php endif; ?>>
        <?php print $item['name']; ?>

        <?php if ($is_active): ?>
        <span class="sr-only"><?php print t('(current)'); ?></span>
        <?php endif; ?>

        <?php if ($has_children): ?>
        <span class="caret"></span>
        <?php endif; ?>
      </a>
      <?php if ($has_children): ?>
      <ul class="dropdown-menu">
        <?php foreach ($item['children'] as $child): ?>
        <li<?php if ($child['active_trail'] == TRUE): ?> class="active"<?php endif; ?>>
          <a href="<?php print url($child['path']); ?>">
            <?php print $child['name']; ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>
    </li>
    <?php endforeach; ?>
  </ul>
</nav>