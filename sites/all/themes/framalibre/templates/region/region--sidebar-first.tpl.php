<?php
/**
 * @file
 * Framalibre theme implementation to display the sidebar_first region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php if ($content): ?>
<div id="region-<?php print str_replace('_', '-', $region); ?>" class="<?php print $classes; ?>">
  <div class="row">
    <div class="col-xs-6 hidden-md hidden-lg">
      <a class="btn btn-default btn-block" role="button" href="#region-sidebar-first .block-taxonomy-menu-block" data-toggle="collapse" aria-expanded="false">
        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
        <?php print t('Toggle secondary menu'); ?>
      </a>
    </div>
    <div class="col-xs-6 col-md-12">
      <a href="<?php print $alternatives_page_url; ?>" class="btn btn-primary btn-block">
        <?php print t('Alternative softwares'); ?>
      </a>
    </div>
  </div>
  <?php print $content; ?>
</div>
<?php endif; ?>