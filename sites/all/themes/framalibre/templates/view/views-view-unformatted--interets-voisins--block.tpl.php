<?php
/**
 * @file
 * Framalibre view template to display rows of the close interests view (block display mode).
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($rows)): ?>
<?php if (!empty($title)): ?>
<h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="nodes-list">
  <?php foreach (array_chunk($rows, 3, TRUE) as $row): ?>
  <div class="nodes-list-row">
    <?php foreach ($row as $item): ?>
    <?php print $item; ?>
    <?php endforeach; ?>

    <?php for ($i = 0; $i < 3 - count($row); $i++): ?>
    <div class="nodes-list-empty-item">&nbsp;</div>
    <?php endfor; ?>
  </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>