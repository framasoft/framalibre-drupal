<?php
/**
 * @file
 * Framalibre view template to display rows of the search view (page display mode).
 *
 * @ingroup views_templates
 */
?>
<?php $content_types = node_type_get_types(); ?>
<?php foreach ($groups as $type => $group): ?>
<h2><?php print $content_types[$type]->name ?></h2>
<div class="nodes-list">
  <?php foreach (array_chunk($group, 3) as $row): ?>
  <div class="nodes-list-row">
    <?php foreach ($row as $item): ?>
    <?php print $item; ?>
    <?php endforeach; ?>
    
    <?php for ($i = 0; $i < 3 - count($row); $i++): ?>
    <div class="nodes-list-empty-item">&nbsp;</div>
    <?php endfor; ?>
  </div>
  <?php endforeach; ?>
</div>
<?php endforeach; ?>