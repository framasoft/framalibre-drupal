<?php
/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php
  if (!empty($q)) {
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  }

  // Prepares widgets
  if (count($widgets) > 2) {
    $widget_rows = array();

    if (isset($widgets['filter-keys'])) {
      $widget_rows[] = array('filter-keys' => $widgets['filter-keys']);
      unset($widgets['filter-keys']);
    }

    $widget_rows = array_merge($widget_rows, array_chunk($widgets, 2, TRUE));
  }
  else {
    $widget_rows = array_chunk($widgets, 2, TRUE);
  }
?>
<div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
    <?php foreach ($widget_rows as $index => $widget_row): ?>
    <?php if ($index == 1): ?>
    <fieldset class="panel panel-primary form-wrapper">
      <legend class="panel-heading">
        <a href="#filters" class="panel-title fieldset-legend<?php print ($collapsed ? ' collapsed' : ''); ?>" data-toggle="collapse">
          <?php print t('More options'); ?>
          <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
          <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
        </a>
      </legend>
      <div id="filters" class="panel-collapse collapse<?php print (!$collapsed ? ' in' : ''); ?>">
        <div class="panel-body">
    <?php endif; ?>

          <div class="views-exposed-widget-row">
            <?php foreach ($widget_row as $id => $widget): ?>
            <?php if (is_object($widget) && !empty($widget->widget)): ?>
            <div id="<?php print $widget->id; ?>-wrapper" class="views-exposed-widget views-widget-<?php print $id; ?>">
              <?php if (!empty($widget->label)): ?>
              <label for="<?php print $widget->id; ?>">
                <?php print $widget->label; ?>
              </label>
              <?php endif; ?>

              <?php if (!empty($widget->operator)): ?>
              <div class="views-operator">
                <?php print $widget->operator; ?>
              </div>
              <?php endif; ?>

              <div class="views-widget">
                <?php print $widget->widget; ?>
              </div>

              <?php if (!empty($widget->description)): ?>
              <div class="description">
                <?php print $widget->description; ?>
              </div>
              <?php endif; ?>
            </div>
            <?php if ($id == 'filter-keys'): ?>
            <div class="views-exposed-widget views-widget-button">
              <?php print $keys_submit; ?>
            </div>
            <?php endif; ?>
            <?php elseif (is_string($widget)): ?>
            <div class="views-exposed-widget views-widget-<?php print $id; ?>">
              <?php print $widget; ?>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
          </div>

    <?php if ($index > 1 && $index == count($widget_rows) - 1): ?>
        </div>
      </div>
    </fieldset>
    <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>
