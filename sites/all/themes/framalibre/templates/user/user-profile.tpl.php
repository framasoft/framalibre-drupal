<?php
/**
 * @file
 * Framalibre theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>
<div class="profile"<?php print $attributes; ?>>
  <div class="media">
    <div class="media-left">
      <?php print render($user_profile['user_picture']); ?>
    </div>
    <div class="media-body">
      <div class="page-header">
        <h1 class="media-heading"><?php print $elements['#account']->name; ?></h1>
        <!-- USER JOB ------------>
        <?php if (isset($user_profile['field_profession'])): ?>
          <?php print render($user_profile['field_profession']) ?>
        <?php endif; ?>
        <em><?php print t("Member for") . ' ' . render($user_profile['member_for']); ?></em>
      </div>
      <div class="user-infos">
        <!-- USER BIOGRAPHIE ------------>
        <?php if (isset($user_profile['field_biographie'])): ?>
          <?php print render($user_profile['field_biographie']); ?>
        <?php endif; ?>

        <div class="user-infos-links">
          <!-- USER'S WEBSITE ------------>
          <?php if (isset($website_url)): ?>
          <?php print t("See more informations on <a href=\"@url\" target=\"_blank\">the author's website</a>.", array('@url' => $website_url)); ?>
          <br/><br/>
          <?php endif; ?>

          <!-- USER'S TWITTER ------------>
          <?php if (isset($twitter_url)): ?>
          <?php
            print t("Follow this author on Twitter: <a href=\"@url\" target=\"_blank\">@text</a>.", array(
              '@url' => $twitter_url,
              '@text' => $twitter_name,
            ));
          ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <h2 class="block-title"><?php print t("Last written sheets"); ?></h2>
      <?php if (isset($user_profile['owned_sheets'])): ?>
        <?php print render($user_profile['owned_sheets']); ?>
      <?php endif; ?>
    </div>
    <div class="col-sm-6">
      <h2 class="block-title"><?php print t("Last bookmarked sheets"); ?></h2>
      <?php if (isset($user_profile['bookmarked_sheets'])): ?>
        <?php print render($user_profile['bookmarked_sheets']); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
