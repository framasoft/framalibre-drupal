<?php if ($label): ?>
<div<?php print drupal_attributes($attributes); ?>>
  <button type="button" class="btn btn-<?php print $type; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php print $label; ?> <span class="caret"></span>
  </button>
  <?php print $dropdown_menu; ?>
</div>
<?php endif; ?>
