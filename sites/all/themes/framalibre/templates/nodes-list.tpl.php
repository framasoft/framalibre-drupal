<?php
/**
 * @file
 * Framalibre theme implementation to display a nodes list.
 *
 * Available variables:
 *
 * - $nodes: an array of the nodes to display.
 * - $view_mode: the view mode desired by the caller of the theme function.
 * - $row_size: the number of nodes to display in each rows.
 * - $block: the block in which the nodes list will be displayed (if block context).
 *
 * @ingroup themeable
 */
?>
<div class="nodes-list nodes-list-<?php print $view_mode; ?>">
  <?php foreach (array_chunk($nodes, $row_size, true) as $row): ?>
  <div class="nodes-list-row">
    <?php $row_view = node_view_multiple($row, $view_mode); ?>
    <?php print render($row_view); ?>
    <?php for ($i = 0; $i < $row_size - count($row); $i++): ?>
    <div class="nodes-list-empty-item">&nbsp;</div>
    <?php endfor; ?>
  </div>
  <?php endforeach; ?>
</div>