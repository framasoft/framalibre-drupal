<?php
/**
 * @file
 * Theme settings form
 */


/**
 * Implements hook_form_system_theme_settings_alter().
 */
function framalibre_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['framalibre'] = array(
    '#type' => 'fieldset',
    '#title' => t('Framalibre settings'),
    '#tree' => FALSE,
    '#collapsible' => FALSE,
    '#weight' => -100,
  );
  
  // Adds a text area to specify paths for which the sidebar must be placed on the right
  $form['framalibre']['framalibre_sidebar_on_right'] = array(
    '#type' => 'textarea',
    '#title' => t('Display the sidebar on the right for the following paths:'),
    '#default_value' => theme_get_setting('framalibre_sidebar_on_right'),
    '#description' => t('Path patterns for which the sidebar must placed on the right of the main content. Each one must be provided on a new line.'),
  );
  
  // Adds a checkbox to toggle usage of the framanav's bootstrap stylesheets
  $form['framalibre']['framalibre_use_framanav_bootstrap_styles'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use framanav\'s bootstrap stylesheets'),
    '#default_value' => theme_get_setting('framalibre_use_framanav_bootstrap_styles'),
    '#description' => t('Warning: enable this setting could cause some style differences.'),
  );
  
  // Adds a checkbox to toggle usage of the framanav's bootstrap scripts
  $form['framalibre']['framalibre_use_framanav_bootstrap_scripts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use framanav\'s bootstrap scripts'),
    '#default_value' => theme_get_setting('framalibre_use_framanav_bootstrap_scripts'),
    '#description' => t('Warning: enable this setting could cause some bugs if the version or the content of the scripts are different.'),
  );
}