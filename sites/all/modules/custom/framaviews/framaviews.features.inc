<?php
/**
 * @file
 * framaviews.features.inc
 */

/**
 * Implements hook_views_api().
 */
function framaviews_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
