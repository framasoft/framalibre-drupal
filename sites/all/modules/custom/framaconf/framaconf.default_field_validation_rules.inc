<?php
/**
 * @file
 * framaconf.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function framaconf_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur de la notice logiciel';
  $rule->name = 'longueur_de_la_notice_logiciel';
  $rule->field_name = 'body';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'logiciel_annuaires';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '200',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 200 car.).';
  $export['longueur_de_la_notice_logiciel'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur de la notice materiel';
  $rule->name = 'longueur_de_la_notice_materiel';
  $rule->field_name = 'body';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'materiel';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '200',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 200 car.).';
  $export['longueur_de_la_notice_materiel'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur du resume artblogjournal';
  $rule->name = 'longueur_du_resume_artblogjourna';
  $rule->field_name = 'body';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'art_revueblogchapitrejournal';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '400',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 400 car.).';
  $export['longueur_du_resume_artblogjourna'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur du résumé livre';
  $rule->name = 'longueur_du_resume_livre';
  $rule->field_name = 'field_r_sum_du_livre';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'livre';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '500',
    'max' => '1600',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 500 car.).';
  $export['longueur_du_resume_livre'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur notice media';
  $rule->name = 'longueur_notice_media';
  $rule->field_name = 'body';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'media';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '200',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 200 car.).';
  $export['longueur_notice_media'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Longueur notice non specifique';
  $rule->name = 'longueur_notice_non_specifique';
  $rule->field_name = 'body';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'ressource_non_specif';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '200',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      5 => 0,
    ),
    'errors' => 0,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Hum, votre descriptif ne contient que [length] caractères. Faites encore un petit effort ! (au moins 200 car.).';
  $export['longueur_notice_non_specifique'] = $rule;

  return $export;
}
