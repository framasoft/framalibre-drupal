<?php
/**
 * @file
 * framaconf.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function framaconf_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_approuve_par_framasoft'.
  $permissions['create field_approuve_par_framasoft'] = array(
    'name' => 'create field_approuve_par_framasoft',
    'roles' => array(
      'Framarevizor' => 'Framarevizor',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_concerne_framasoft'.
  $permissions['create field_concerne_framasoft'] = array(
    'name' => 'create field_concerne_framasoft',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_approuve_par_framasoft'.
  $permissions['edit field_approuve_par_framasoft'] = array(
    'name' => 'edit field_approuve_par_framasoft',
    'roles' => array(
      'Framarevizor' => 'Framarevizor',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_concerne_framasoft'.
  $permissions['edit field_concerne_framasoft'] = array(
    'name' => 'edit field_concerne_framasoft',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_approuve_par_framasoft'.
  $permissions['edit own field_approuve_par_framasoft'] = array(
    'name' => 'edit own field_approuve_par_framasoft',
    'roles' => array(
      'Framarevizor' => 'Framarevizor',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_concerne_framasoft'.
  $permissions['edit own field_concerne_framasoft'] = array(
    'name' => 'edit own field_concerne_framasoft',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_approuve_par_framasoft'.
  $permissions['view field_approuve_par_framasoft'] = array(
    'name' => 'view field_approuve_par_framasoft',
    'roles' => array(
      'Framarevizor' => 'Framarevizor',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_concerne_framasoft'.
  $permissions['view field_concerne_framasoft'] = array(
    'name' => 'view field_concerne_framasoft',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_approuve_par_framasoft'.
  $permissions['view own field_approuve_par_framasoft'] = array(
    'name' => 'view own field_approuve_par_framasoft',
    'roles' => array(
      'Framarevizor' => 'Framarevizor',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_concerne_framasoft'.
  $permissions['view own field_concerne_framasoft'] = array(
    'name' => 'view own field_concerne_framasoft',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
