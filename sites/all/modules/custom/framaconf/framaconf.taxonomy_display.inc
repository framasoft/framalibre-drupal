<?php
/**
 * @file
 * framaconf.taxonomy_display.inc
 */

/**
 * Implements hook_taxonomy_display_default_displays().
 */
function framaconf_taxonomy_display_default_displays() {
  $export = array();

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'alternatives_proprios';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = FALSE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['alternatives_proprios'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'annuaires';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['annuaires'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'beta';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['beta'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'cr_ateur_s_';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['cr_ateur_s_'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'format_document';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['format_document'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'framapprop';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['framapprop'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'framaprojet';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['framaprojet'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'genre_de_chronique';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['genre_de_chronique'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'langues';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['langues'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'licences';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['licences'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'os';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['os'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'r_f_rencement_interne';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['r_f_rencement_interne'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'societes_organisations';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['societes_organisations'] = $taxonomy_display;

  $taxonomy_display = new stdClass();
  $taxonomy_display->api_version = 1;
  $taxonomy_display->machine_name = 'tags';
  $taxonomy_display->term_display_plugin = 'TaxonomyDisplayChildTermDisplay';
  $taxonomy_display->term_display_options = '';
  $taxonomy_display->associated_display_plugin = 'FramastuffTermAssociatedContentDisplayHandler';
  $taxonomy_display->associated_display_options = '';
  $taxonomy_display->add_feed = TRUE;
  $taxonomy_display->breadcrumb_display_plugin = 'TaxonomyDisplayBreadcrumbDisplayHandlerCore';
  $taxonomy_display->breadcrumb_display_options = '';
  $export['tags'] = $taxonomy_display;

  return $export;
}
