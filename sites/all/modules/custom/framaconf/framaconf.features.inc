<?php
/**
 * @file
 * framaconf.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function framaconf_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_validation" && $api == "default_field_validation_rules") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function framaconf_image_default_styles() {
  $styles = array();

  // Exported image style: alternative_logo.
  $styles['alternative_logo'] = array(
    'label' => 'Alternative logo',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 60,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: icon.
  $styles['icon'] = array(
    'label' => 'Icône',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 20,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: micro_teaser_logo.
  $styles['micro_teaser_logo'] = array(
    'label' => 'Micro teaser logo',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 40,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: screenshot.
  $styles['screenshot'] = array(
    'label' => 'Screenshot',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 767,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teaser_logo.
  $styles['teaser_logo'] = array(
    'label' => 'Teaser logo',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 70,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function framaconf_node_info() {
  $items = array(
    'art_revueblogchapitrejournal' => array(
      'name' => t('Un article'),
      'base' => 'node_content',
      'description' => t('Ajouter un article issu d\'une revue, d\'un journal, d\'un blog, d\'un site...'),
      'has_title' => '1',
      'title_label' => t('Titre de l\'article'),
      'help' => t('Une notice « Article » a pour objectif de recenser un article issu d\'un blog, d\'un journal, d\'une revue, d\'un site, etc. <strong>Important :</strong> des textes d\'aide vous sont proposés pour les champs à remplir.<br />
<span class="label label-danger">Ne recopiez pas les contenus</span> (issus de Wikipédia ou autres sites), écrivez-les vous-même, c\'est mieux!<br />'),
    ),
    'livre' => array(
      'name' => t('Livre'),
      'base' => 'node_content',
      'description' => t('Pour signaler une monographie ou un ouvrage collectif'),
      'has_title' => '1',
      'title_label' => t('Livre'),
      'help' => t('Une notice « Livre » est une contribution à une liste de références libristes. Le résumé de l\'ouvrage est aussi essentiel que les informations bibliographiques. <strong>Important :</strong> Pensez à vérifier les références de l\'ouvrage. Des textes d\'aide vous sont proposés pour les champs à remplir.'),
    ),
    'logiciel_annuaires' => array(
      'name' => t('Logiciel'),
      'base' => 'node_content',
      'description' => t('Ajouter un logiciel libre'),
      'has_title' => '1',
      'title_label' => t('Nom du logiciel'),
      'help' => t('Une notice « Logiciel » permet au lecteur d\'identifier rapidement les informations importantes et les fonctionnalités principales d\'un logiciel libre. Privilégiez les mots-clés (et sans coquilles). <strong>Important :</strong> des textes d\'aide vous sont proposés pour les champs à remplir.<br />


<span class="label label-danger">Ne recopiez pas les contenus</span> (issus de Wikipédia ou autres sites), écrivez-les vous-même, c\'est mieux!<br />

... et relisez-vous :) '),
    ),
    'materiel' => array(
      'name' => t('Matériel'),
      'base' => 'node_content',
      'description' => t('Ajouter une référence pour du matériel, outils, dispositifs, etc.'),
      'has_title' => '1',
      'title_label' => t('Nom du matériel'),
      'help' => t('Une notice « Matériel » permet au lecteur d\'identifier rapidement les caractéristiques importantes du matériel recensé. Privilégiez les mots-clés (et sans coquilles). <strong>Important :</strong> des textes d\'aide vous sont proposés pour les champs à remplir.<br />


<span class="label label-danger">Ne recopiez pas les contenus</span> (issus de Wikipédia ou autres sites), écrivez-les vous-même, c\'est mieux!<br />'),
    ),
    'media' => array(
      'name' => t('Média'),
      'base' => 'node_content',
      'description' => t('Ajouter une ressource multimédia (video, musique, diaporama...)'),
      'has_title' => '1',
      'title_label' => t('Titre de la ressource'),
      'help' => t('Une notice « Média » recense un site web, une vidéo, une bande son, une image, etc.  Soyez attentif aux mots-clés. <strong>Important :</strong> des textes d\'aide vous sont proposés pour les champs à remplir.  <br />

<span class="label label-danger">Ne recopiez pas les contenus</span> (issus de Wikipédia ou autres sites), écrivez-les vous-même, c\'est mieux!<br />'),
    ),
    'ressource_non_specif' => array(
      'name' => t('Ressource générique'),
      'base' => 'node_content',
      'description' => t('Ajouter une ressource (contenu non spécifique)'),
      'has_title' => '1',
      'title_label' => t('Titre de la ressource'),
      'help' => t('Une notice « Ressource générique » est consacrée à la description de ressources dont le type n\'est pas proposé au choix des contributions. Cela n\'enlève rien à son intérêt ! Privilégiez les mots-clés (et sans coquilles). <strong>Important :</strong> des textes d\'aide vous sont proposés pour les champs à remplir.<br />


<span class="label label-danger">Ne recopiez pas les contenus</span> (issus de Wikipédia ou autres sites), écrivez-les vous-même, c\'est mieux!<br />'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
