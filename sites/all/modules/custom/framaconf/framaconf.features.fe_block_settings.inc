<?php
/**
 * @file
 * framaconf.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function framaconf_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['aggregator-feed-1'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'feed-1',
    'module' => 'aggregator',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'framalibre',
        'weight' => -25,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['framastuff-contribute'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'contribute',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'framalibre',
        'weight' => -29,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['framastuff-free_software_references'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'free_software_references',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'framalibre',
        'weight' => -24,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['framastuff-most_popular'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'most_popular',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'framalibre',
        'weight' => -20,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['framastuff-novelties_content'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'novelties_content',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'framalibre',
        'weight' => -24,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['framastuff-novelties_sidebar'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'novelties_sidebar',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '<front>
user
user/*
user/*/favoris',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'framalibre',
        'weight' => -11,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['framastuff-search'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'search',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'framalibre',
        'weight' => -28,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['framastuff-support'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'support',
    'module' => 'framastuff',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'framalibre' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'framalibre',
        'weight' => -7,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
