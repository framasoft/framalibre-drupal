<?php


class NodeLoader extends EntityFieldQuery {

  public function __construct($type) {
    $this->entityCondition('entity_type', 'node');
    $this->entityCondition('bundle', $type);
    $this->addTag('node_access');
  }


  public function execute() {
    $result = parent::execute();

    if ($this->count) {
      return $result;
    }
    if (!empty($result['node'])) {
      return node_load_multiple(array_keys($result['node']));
    }

    return array();
  }


  /**
   * Returns the last published nodes.
   * @param string|array $type Node type
   * @param integer $number Number of nodes to return
   * @return array
   */
  public static function getLastPublished($type, $number) {
    $loader = new self($type);
    $loader->propertyCondition('status', NODE_PUBLISHED);
    $loader->propertyOrderBy('created', 'DESC');
    $loader->range(0, $number);
    return $loader->execute();
  }


  /**
   * Returns the promoted nodes.
   * @param string|array $type Node type
   * @param integer $number Number of nodes to return
   * @return array
   */
  public static function getPromoted($type, $number) {
    $loader = new self($type);
    $loader->propertyCondition('status', NODE_PUBLISHED);
    $loader->fieldCondition('field_approuve_par_framasoft', 'value', 1);
    $loader->propertyOrderBy('changed', 'DESC');
    $loader->range(0, $number);
    return $loader->execute();
  }


  /**
   * Returns the most popular nodes related to a precise category.
   * @param string|array $type Node type
   * @param integer|object $category Taxonomy term of a catagory (or its ID)
   * @param integer $number Number of nodes to return
   * @return array
   */
  public static function getMostPopular($type, $category, $number) {
    $tid = is_object($category) ? $category->tid : $category;

    // Gets all sub-category ids
    $children = self::getAllChildrenTerms($category);

    // Adds the category id to the children ones
    $children[] = $tid;

    $loader = new self($type);
    $loader->propertyCondition('status', NODE_PUBLISHED);
    $loader->fieldCondition('field_annuaires', 'tid', $children, 'IN');
    $loader->propertyOrderBy('changed', 'ASC');
    $loader->range(0, $number);
    $loader->addTag('vote_count_order');
    $loader->addMetaData('vote_count_order_position', 1);
    return $loader->execute();
  }


  /**
   * Returns the nodes associated with a specific category.
   * @param string|array $type Node type
   * @param integer|object $category Taxonomy term of a catagory (or its ID)
   * @param integer $number Number of nodes to return
   * @param array $filters Definition of additional filters
   * @param boolean $pager TRUE to use the pager system
   * @return array
   */
  public static function getAssociatedWithCategory($type, $category, $number, $filters = array(), $pager = TRUE) {
    $tid = is_object($category) ? $category->tid : $category;

    $children = self::getAllChildrenTerms($category);

    $loader = new self($type);
    $loader->propertyCondition('status', NODE_PUBLISHED);
    $loader->fieldCondition('field_annuaires', 'tid', $children, 'IN');
    $loader->propertyOrderBy('title', 'ASC');

    foreach ($filters as $filter) {
      $operator = !empty($filter['operator']) ? $filter['operator'] : '=';

      if (isset($filter['field'])) {
        $loader->fieldCondition($filter['field'], $filter['column'], $filter['value'], $operator);
      } else {
        $loader->propertyCondition($filter['property'], $filter['value'], $operator);
      }
    }

    if ($pager) {
      $loader->pager($number);
    } else {
      $loader->range(0, $number);
    }

    return $loader->execute();
  }


  /**
   * Returns nodes associated with a given term.
   *
   * @param string|array $type Node type
   * @param object $term Taxonomy term
   * @param integer $number Number of nodes to return
   * @param boolean $pager TRUE to use the pager system
   * @return array
   */
  public static function getAssociatedWithTerm($type, $term, $number, $pager = TRUE) {
    if (!variable_get('taxonomy_maintain_index_table', TRUE)) {
      return array();
    }

    $tid = is_object($term) ? $term->tid : $term;

    $query = db_select('taxonomy_index', 't');
    $query->join('node', 'n', "n.nid = t.nid");
    $query->addTag('node_access');
    $query->condition('tid', $tid);

    if ($pager) {
      $count_query = clone $query;
      $count_query->addExpression('COUNT(t.nid)');

      $query = $query->extend('PagerDefault')->limit($number);
      $query->setCountQuery($count_query);
    }
    else {
      $query->range(0, $number);
    }

    $query->addField('t', 'nid');
    $query->orderBy('n.title', 'ASC');

    $identifiers = $query->execute()->fetchCol();
    $nodes = array();

    if ($identifiers) {
      $nodes = node_load_multiple($identifiers);
    }

    return $nodes;
  }


  /**
   * Returns nodes owned by a given user.
   * @param string|array $type Node type
   * @param integer|object $account The owner account
   * @param integer $number Number of nodes to return
   * @return array
   */
  public static function getOwnedBy($type, $account, $number) {
    $uid = is_object($account) ? $account->uid : $account;

    $loader = new self($type);
    $loader->propertyCondition('uid', $uid);
    $loader->propertyOrderBy('created', 'DESC');
    $loader->range(0, $number);

    return $loader->execute();
  }


  /**
   * Returns nodes bookmarked by a given user.
   * @param string|array $type Node type
   * @param integer|object $account The user account
   * @param integer $number Number of nodes to return
   * @return array
   */
  public static function getBookmarkedBy($type, $account, $number) {
    $uid = is_object($account) ? $account->uid : $account;

    $loader = new self($type);
    $loader->range(0, $number);

    $loader->addTag('flagged');
    $loader->addMetaData('flag_name', 'bookmarks');
    $loader->addMetaData('flagged_by', $uid);

    return $loader->execute();
  }

  /**
   * Wrapper function to recursively return all children terms using
   * taxonomy_get_tree, instead of taxonomy_get_children which is limited to
   * level-1 children.
   *
   * @param [type] $category
   * @return void
   */
  public static function getAllChildrenTerms($category) {
    // Gets all sub-category ids
    $term_objects = taxonomy_get_tree($category->vid, $category->tid, NULL, FALSE);

    $children = [];
    foreach($term_objects as $child) {
      $children[] = $child->tid;
    }
    $children[] = $category->tid;
    return $children;
  }
}


// /**
//  * Implements hook_query_TAG_alter().
//  */
function framastuff_query_vote_count_order_alter(QueryAlterableInterface $query) {
  // Changes joint type for the field_data_field_votre_appr_ciation table
  // $tables = &$query->getTables();
  // foreach ($tables as &$table) {
  //   if ($table['table'] == 'field_data_field_votre_appr_ciation') {
  //     $table['join type'] = 'LEFT';
  //   }
  // }

  // Joins the votes table
  $query->leftJoin('votingapi_vote', 'vv', "vv.entity_type = 'node' AND vv.entity_id = node.nid");
  // Counts votes
  $query->addExpression('count(vv.vote_id)');
  // Groups by all nonaggregated columns present
  // in SELECT and ORDER BY lists
  $query->groupBy('field_data_field_annuaires0.entity_id');
  $query->groupBy('field_data_field_annuaires0.entity_type');
  $query->groupBy('field_data_field_annuaires0.bundle');
  $query->groupBy('field_data_field_annuaires0.revision_id');

  foreach ($query->getOrderBy() as $field => $order) {
    $query->groupBy($field);
  }

  // Adds the order clause to the wanted position
  $order_position = $query->getMetaData('vote_count_order_position');
  if ($order_position !== NULL) {
    $order = &$query->getOrderBy();
    $temp = array_splice($order, $order_position);
    $query->orderBy('count(vv.vote_id)', 'DESC');
    $order = array_merge($order, $temp);
  } else {
    $query->orderBy('count(vv.vote_id)', 'DESC');
  }
}


/**
 * Implements hook_query_TAG_alter().
 */
function framastuff_query_flagged_alter(QueryAlterableInterface $query) {
  $flag = $query->getMetaData('flag_name');
  $account = $query->getMetaData('flagged_by');

  $query->join('flagging', 'fg', "fg.entity_type = 'node' AND fg.entity_id = node.nid");
  $query->join('flag', 'f', "f.fid = fg.fid");
  $query->condition('f.name', $flag);

  if ($account !== null) {
    $uid = is_object($account) ? $account->uid : $account;
    $query->condition('fg.uid', $uid);
  }

  $query->orderBy('fg.timestamp', 'DESC');
}
