(function ($) {

  Drupal.behaviors.directoryFilters = {
    attach: function (context, settings) {
      var $filtersForm = $('#framastuff-taxonomy-term-filters-form');

      $filtersForm.find('.form-radio').change(function(event) {
        $filtersForm.submit();
      });
    }
  };

})(jQuery);