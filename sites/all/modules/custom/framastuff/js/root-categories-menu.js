(function ($) {

  Drupal.behaviors.rootCategoriesMenu = {
    attach: function (context, settings) {
      var $currentMenu = $();

      // Shows sub-menu (and hides the current one)
      $('#block-framastuff-root-categories-menu .root-term', context).click(function(event) {
        event.preventDefault();
        
        $menu = $(this).siblings('.sub-terms');

        if ($menu != $currentMenu) {
          $currentMenu.hide({
            effect: 'slide',
            direction: 'left'
          });

          $menu.show({
            effect: 'slide',
            direction: 'left'
          });

          $currentMenu = $menu;
        }
      });

      // Hides sub-menu
      $('#block-framastuff-root-categories-menu .close', context).click(function(event) {
        $(this).closest('.sub-terms').hide({
          effect: 'slide',
          direction: 'left'
        });

        $currentMenu = $();
      });
    }
  };

})(jQuery);