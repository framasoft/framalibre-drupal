<?php
/**
 * @file
 * Theme implementation to display the alternatives page.
 *
 * @ingroup themeable
 */
?>
<?php print drupal_render($search_form); ?>

<div class="alternatives">
  <?php $prev_cat = $prev_prop = NULL; ?>

  <?php foreach ($alternatives as $alt): ?>
    <?php if ($alt['prop_software'] != $prev_prop && isset($prev_prop)): ?>
    </div><!-- Closes right column -->
  </div><!-- Closes row -->
    <?php endif; ?>

    <?php if ($alt['category'] != $prev_cat): ?>
      <?php if (isset($prev_cat)): ?>
      </div><!-- Closes well div -->
      <?php endif; ?>

    <h2><?php echo $alt['category']->name; ?></h2>
    <div class="well">
    <?php endif; ?>

    <?php if ($alt['prop_software'] != $prev_prop): ?>
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-5">
        <?php
          $prop_view = taxonomy_term_view($alt['prop_software'], 'alternative');
          print render($prop_view);
        ?>
      </div>

      <div class="col-lg-7 col-md-7 col-sm-7 altpadleft">
        <span class="glyphicon glyphicon-menu-right"></span>
    <?php endif; ?>
    <?php
      $free_view = node_view($alt['free_software'], 'alternative');
      print render($free_view);
    ?>
    <?php $prev_cat  = $alt['category']; ?>
    <?php $prev_prop = $alt['prop_software']; ?>
  <?php endforeach; ?>

  <?php if (!empty($alternatives)): ?>
      </div><!-- Closes last right column -->
    </div><!-- Closes last row -->
  </div><!-- Closes last well div -->
  <?php else: ?>
  <div class="alert alert-info">
    <?php print t("No result matching your search."); ?>
  </div>
  <?php endif; ?>
</div>