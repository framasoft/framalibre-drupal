<?php
/**
 * @file
 * Default theme implementation to display a nodes list.
 *
 * Available variables:
 *
 * - $nodes: an array of the nodes to display.
 * - $view_mode: the view mode desired by the caller of the theme function.
 * - $block: the block in which the nodes list wiil be displayed (if block context).
 *
 * @ingroup themeable
 */
?>
<div class="nodes-list nodes-list-<?php print $view_mode; ?>">
  <?php if (count($nodes)): ?>
  <?php $list = node_view_multiple($nodes, $view_mode); ?>
  <?php print render($list); ?>
  <?php endif; ?>
</div>