<?php


//----------------------------------------------------------------------
//-- MENU & PERMISSIONS
//----------------------------------------------------------------------


/**
 * Implements hook_menu().
 */
function framastuff_menu() {
  $items = array();

  $items['home'] = array(
    'title' => 'Home',
    'page callback' => 'framastuff_page_home',
    'access arguments' => array('access content'),
  );

  $items['alternative-free-softwares'] = array(
    'title' => 'Alternative softwares',
    'page callback' => 'framastuff_page_alternatives',
    'access arguments' => array('access content'),
  );

  $items['proprietary-softwares'] = array(
    'page callback' => 'framastuff_autocompletion_proprietary_softwares',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['user/%user/favoris'] = array(
    'title callback' => 'framastuff_menu_title_user_bookmarks',
    'title arguments' => array(1),
    'page callback' => 'framastuff_page_user_bookmarks',
    'page arguments' => array(1),
    'access callback' => 'framastuff_access_user_bookmarks',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  $items['user/favoris'] = array(
    'title' => "My bookmarks",
    'page callback' => 'framastuff_goto_user_bookmarks',
    'access callback' => 'user_is_logged_in',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}


/**
 * Implements hook_menu_alter().
 */
function framastuff_menu_alter(&$items) {
  $items['user/%user/view']['title'] = "View profile";
}


/**
 * Access callback for user's bookmarks page.
 */
function framastuff_access_user_bookmarks($account) {
  if ($account->uid == $GLOBALS['user']->uid) {
    return TRUE;
  }
  $public = field_get_items('user', $account, 'field_rendre_mon_registre_public');
  return (bool) $public[0]['value'];
}


//----------------------------------------------------------------------
//-- PAGES
//----------------------------------------------------------------------


/**
 * Callback for an empty home page.
 */
function framastuff_page_home() {
  return '';
}


/**
 * Title callback of the menu item for user's bookmarks.
 */
function framastuff_menu_title_user_bookmarks($account) {
  if ($account->uid == $GLOBALS['user']->uid) {
    return t("My bookmarks");
  }
  return t("Bookmarks");
}


/**
 * Page title function for user's bookmarks page.
 * @see framastuff_page_user_bookmarks().
 */
function framastuff_page_title_user_bookmarks($account) {
  if ($account->uid == $GLOBALS['user']->uid) {
    return t("My bookmarks");
  }
  return t("Bookmarks of @username", array('@username' => $account->name));
}


/**
 * Page callback for user's bookmarks page.
 */
function framastuff_page_user_bookmarks($account) {
  $page_title = framastuff_page_title_user_bookmarks($account);
  drupal_set_title($page_title);

  $view = views_get_view('framaviews_user_bookmarks');
  $view->set_display('Registre');
  $view->set_arguments(array($account->uid));
  $view->pre_execute();
  $view->execute();

  return $view->render();
}


/**
 * Page callback for the current user's bookmarks page.
 */
function framastuff_goto_user_bookmarks() {
  global $user;
  drupal_goto('user/' . $user->uid . '/favoris', array(), 303);
}


/**
 * Callback for the alternatives page.
 */
function framastuff_page_alternatives() {

  $software = !empty($_GET['software']) ? $_GET['software'] : NULL;
  $alternatives = _framastuff_get_alternatives($software);

  if ($alternatives) {
    $free_software_ids = $prop_software_ids = $category_ids = array();

    foreach ($alternatives as $alt) {
      $free_software_ids[] = $alt['free_software_nid'];
      $prop_software_ids[] = $alt['prop_software_tid'];
      $category_ids[] = $alt['category_tid'];
    }

    $free_softwares = node_load_multiple($free_software_ids);
    $prop_softwares = taxonomy_term_load_multiple($prop_software_ids);
    $categories = taxonomy_term_load_multiple($category_ids);

    foreach ($alternatives as &$alt) {
      $alt = array(
        'free_software' => $free_softwares[$alt['free_software_nid']],
        'prop_software' => $prop_softwares[$alt['prop_software_tid']],
        'category' => $categories[$alt['category_tid']],
      );
    }
  }
  elseif ($software) {
    form_set_error('software', t("Unrecognized software: please select a software in the suggestions list."));
  }

  return array(
    'alternatives' => array(
      '#theme' => 'page_alternatives',
      '#search_form' => drupal_get_form('framastuff_alternatives_search_form'),
      '#alternatives' => $alternatives,
    ),
    'pager' => array(
      '#theme' => 'pager',
    ),
  );
}


/**
 * Callback for the proprietary softwares autocompletion.
 */
function framastuff_autocompletion_proprietary_softwares($string = '') {
  $matches = array();

  if ($string = trim($string)) {
    $query = db_select('taxonomy_term_data', 'ttd');
    $query->join('taxonomy_vocabulary', 'tv', 'ttd.vid = tv.vid');
    $query->fields('ttd', array('name'))
      ->condition('tv.machine_name', 'alternatives_proprios')
      ->condition('ttd.name', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, 10);

    foreach ($query->execute()->fetchCol() as $term_name) {
      $matches[$term_name] = check_plain($term_name);
    }
  }

  drupal_json_output($matches);
}


//----------------------------------------------------------------------
//-- BLOCKS
//----------------------------------------------------------------------


/**
 * Implements hook_block_info().
 */
function framastuff_block_info() {
  $blocks = array();

  $blocks['novelties_content'] = array(
    'info' => t('Novelties (Content)'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  $blocks['novelties_sidebar'] = array(
    'info' => t('Novelties (Sidebar)'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  $blocks['free_software_references'] = array(
    'info' => t('Our references'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  $blocks['most_popular'] = array(
    'info' => t('The most popular'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  $blocks['support'] = array(
    'info' => t('Support Framasoft'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  $blocks['contribute'] = array(
    'info' => t('Contribute'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );

  $blocks['search'] = array(
    'info' => t('Search field'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function framastuff_block_view($delta = '') {
  $callback = 'framastuff_block_' . $delta . '_view';

  if (function_exists($callback)) {
    return call_user_func($callback);
  }

  return array();
}


/**
 * Callback for the novelties_content block.
 */
function framastuff_block_novelties_content_view() {
  $content_types = _framastuff_get_sheet_content_types();

  if ($novelties = NodeLoader::getLastPublished($content_types, 3)) {
    return array(
      'subject' => t('Last contributions'),
      'content' => array(
        'nodes' => array(
          '#theme' => 'nodes_list',
          '#nodes' => $novelties,
          '#view_mode' => 'teaser',
          '#block' => block_load('framastuff', 'novelties_content'),
        ),
      ),
    );
  }
}


/**
 * Callback for the novelties_sidebar block.
 */
function framastuff_block_novelties_sidebar_view() {
  $content_types = _framastuff_get_sheet_content_types();

  if ($novelties = NodeLoader::getLastPublished($content_types, 5)) {
    return array(
      'subject' => t('Novelties'),
      'content' => array(
        'nodes' => array(
          '#theme' => 'nodes_list',
          '#nodes' => $novelties,
          '#view_mode' => 'micro_teaser',
          '#block' => block_load('framastuff', 'novelties_sidebar'),
        ),
      ),
    );
  }
}


/**
 * Callback for the free_software_references block.
 */
function framastuff_block_free_software_references_view() {
  $content_types = _framastuff_get_sheet_content_types();

  if ($references = NodeLoader::getPromoted($content_types, 6)) {
    return array(
      'subject' => t('Our references'),
      'content' => array(
        'nodes' => array(
          '#theme' => 'nodes_list',
          '#nodes' => $references,
          '#view_mode' => 'teaser',
          '#block' => block_load('framastuff', 'free_software_references'),
        ),
      ),
    );
  }
}


/**
 * Callback for the most_popular block.
 */
function framastuff_block_most_popular_view() {
  $content_types = _framastuff_get_sheet_content_types();

  if (
    ($term = menu_get_object('taxonomy_term', 2)) &&
    ($most_popular = NodeLoader::getMostPopular($content_types, $term, 3))
  ) {
    return array(
      'subject' => t('The most popular'),
      'content' => array(
        'nodes' => array(
          '#theme' => 'nodes_list',
          '#nodes' => $most_popular,
          '#view_mode' => 'teaser',
          '#block' => block_load('framastuff', 'most_popular'),
        ),
      ),
    );
  }
}


/**
 * Callback for the support block.
 */
function framastuff_block_support_view() {

  $data = variable_get('framastuff_block_support', array(
    'text' => array('value' => '', 'format' => 'filtered_html'),
    'donation_url' => '',
  ));

  return array(
    'subject' => t('Support Framasoft'),
    'content' => array(
      'text' => array(
        '#markup' => check_markup($data['text']['value'], $data['text']['format']),
      ),
      'link' => array(
        '#theme' => 'link',
        '#text' => t('Make a donation'),
        '#path' => $data['donation_url'],
        '#options' => array(
          'html' => FALSE,
          'attributes' => array(
            'class' => array('btn btn-soutenir'),
          ),
        ),
        '#prefix' => '<div class="link-wrapper">',
        '#suffix' => '</div>',
      ),
    ),
  );
}


/**
 * Callback for the "contribute" block.
 */
function framastuff_block_contribute_view() {

  $data = variable_get('framastuff_block_contribute', array(
    'text'       => array('value' => '', 'format' => 'filtered_html'),
    'help_title' => '',
    'help_text'  => array('value' => '', 'format' => 'filtered_html'),
  ));

  $block = array(
    'subject' => t('Contribute'),
    'content' => array(
      'text' => array('#markup' => check_markup($data['text']['value'], $data['text']['format'])),
    ),
  );

  if (user_is_logged_in()) {
    $block['content'] += array(
      'dropdown' => _framastuff_get_contribute_button_view(),
    );
  }
  else {
    $block['content'] += array(
      'actions' => array(
        '#prefix' => '<div class="actions-wrapper">',
        '#suffix' => '</div>',
        'login' => array(
          '#theme' => 'link',
          '#text' => t('Log in'),
          '#path' => 'user/login',
          '#options' => array(
            'html' => FALSE,
            'attributes' => array('class' => array('btn', 'btn-primary', 'btn-block')),
          ),
        ),
        'register' => array(
          '#theme' => 'link',
          '#text' => t('Create an account'),
          '#path' => 'user/register',
          '#options' => array(
            'html' => FALSE,
            'attributes' => array('class' => array('btn', 'btn-primary', 'btn-block')),
          ),
        ),
      ),
      'help' => array(
        '#prefix' => '<div class="help-wrapper">',
        '#suffix' => '</div>',
        'why' => array(
          '#theme' => 'html_tag',
          '#tag'   => 'a',
          '#value' => '<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>',
          '#attributes' => array(
            'title' => check_plain($data['help_title']),
            'role' => 'button',
            'tabindex' => 0,
            'class' => array('text-info'),
            'data-toggle' => 'popover',
            'data-trigger' => 'focus',
            'data-placement' => 'left',
            'data-html' => 'true',
            'data-content' => check_markup($data['help_text']['value'], $data['help_text']['format']),
          ),
        ),
      ),
    );
  }

  return $block;
}


/**
 * Callback for the "search" block.
 */
function framastuff_block_search_view() {
  $view = views_get_view('recherche_par_criteres');
  $view->block_mode = TRUE;
  $view->set_display('page');
  $view->init_handlers();
  $exposed_form_plugin = $view->display_handler->get_plugin('exposed_form');
  return array('content' => $exposed_form_plugin->render_exposed_form(TRUE));
}


/**
 * Implements hook_block_configure().
 */
function framastuff_block_configure($delta = '') {
  $form = array();

  if ($delta == 'support') {
    $data = variable_get('framastuff_block_support', array(
      'text' => array('value' => '', 'format' => 'filtered_html'),
      'donation_url' => '',
    ));

    $form['text'] = array(
      '#type' => 'text_format',
      '#title' => t('Block text'),
      '#default_value' => $data['text']['value'],
      '#format' => $data['text']['format'],
    );

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('Donation page URL'),
      '#default_value' => $data['donation_url'],
    );
  }
  elseif ($delta == 'contribute') {
    $data = variable_get('framastuff_block_contribute', array(
      'text'       => array('value' => '', 'format' => 'filtered_html'),
      'help_title' => '',
      'help_text'  => array('value' => '', 'format' => 'filtered_html'),
    ));

    $form['text'] = array(
      '#type' => 'text_format',
      '#title' => t('Block text'),
      '#default_value' => $data['text']['value'],
      '#format' => $data['text']['format'],
    );

    $form['help_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Help title'),
      '#default_value' => $data['help_title'],
    );

    $form['help_text'] = array(
      '#type' => 'text_format',
      '#title' => t('Help text'),
      '#default_value' => $data['help_text']['value'],
      '#format' => $data['help_text']['format'],
    );
  }

  return $form;
}


/**
 * Implements hook_block_save().
 */
function framastuff_block_save($delta = '', $edit = array()) {
  if ($delta == 'support') {
    $data = array(
      'text' => $edit['text'],
      'donation_url' => $edit['url'],
    );

    variable_set('framastuff_block_support', $data);
  }
  elseif ($delta == 'contribute') {
    variable_set('framastuff_block_contribute', $edit);
  }
}


/**
 * Implements hook_block_view_alter().
 */
function framastuff_block_view_alter(&$data, $block) {
  if ($block->module == 'aggregator' && !empty($data['content'])) {
    list($type, $fid) = explode('-', $block->delta);
    $feed = aggregator_feed_load($fid);

    // If we're in the context of the framablog feed block,
    // we replace the more link by a link to framablog.
    if (strpos($feed->link, 'framablog.org') !== FALSE) {
      $pattern = '/(<div class="more-link">).*(<\/div>)$/i';
      $replacement = '$1' . l(t('Access the Framablog'), $feed->link, array('external' => TRUE)) . '$2';
      $data['content'] = preg_replace($pattern, $replacement, $data['content']);
    }
  }
}


//----------------------------------------------------------------------
//-- ENTITIES & FIELDS
//----------------------------------------------------------------------


/**
 * Implements hook_field_formatter_info().
 */
function framastuff_field_formatter_info() {
  $formatters = array();

  $formatters['framastuff_term_logo'] = array(
    'label' => t('Logo'),
    'description' => t('Displays the logo of the taxonomy term (or the term itself if no logo is defined)'),
    'field types' => array('taxonomy_term_reference'),
    'settings' => array(
      'image_style' => '',
      'link_to_term_page' => FALSE,
    ),
  );

  return $formatters;
}


/**
 * Implements hook_field_formatter_settings_form().
 */
function framastuff_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'framastuff_term_logo') {
    $image_styles = image_style_options(FALSE, PASS_THROUGH);

    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $settings['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    $element['link_to_term_page'] = array(
      '#title' => t('Link to content'),
      '#type' => 'checkbox',
      '#default_value' => $settings['link_to_term_page'],
    );
  }

  return $element;
}


/**
 * Implements hook_field_formatter_settings_summary().
 */
function framastuff_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'framastuff_term_logo') {
    $summary = array();

    $image_styles = image_style_options(FALSE, PASS_THROUGH);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$settings['image_style']])) {
      $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['image_style']]));
    }
    else {
      $summary[] = t('Original image');
    }

    $summary[] = t('Linked to the term page:') . ' ' . (($settings['link_to_term_page']) ? t('yes') : t('no'));
  }

  return implode('<br/>', $summary);
}


/**
 * Implements hook_field_formatter_view().
 */
function framastuff_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($display['type'] == 'framastuff_term_logo') {
    $term_ids = array();

    foreach ($items as $item) {
      $term_ids[] = $item['tid'];
    }

    $terms = taxonomy_term_load_multiple($term_ids);

    foreach ($items as $delta => $item) {
      $logo = field_get_items('taxonomy_term', $terms[$item['tid']], 'field_logo');

      if (!empty($logo)) {
        if ($display['settings']['link_to_term_page']) {
          $uri = entity_uri('taxonomy_term', $terms[$item['tid']]);
        }

        $element[$delta] = array(
          '#theme' => 'image_formatter',
          '#item' => $logo[0],
          '#image_style' => $display['settings']['image_style'],
          '#path' => isset($uri) ? $uri : '',
        );
      }
      else {
        if ($display['settings']['link_to_term_page']) {
          $display['type'] = 'taxonomy_term_reference_link';
        } else {
          $display['type'] = 'taxonomy_term_reference_plain';
        }

        $item_view = taxonomy_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, array($item), $display);
        $element[$delta] = reset($item_view);
      }
    }
  }

  return $element;
}


/**
 * Implements hook_entity_info_alter().
 */
function framastuff_entity_info_alter(&$entity_info) {
  // Adds view modes for the node entities
  $entity_info['node']['view modes'] += array(
    'micro_teaser' => array(
      'label' => t('Micro teaser'),
      'custom settings' => FALSE,
    ),
    'simple_teaser' => array(
      'label' => t('Simple teaser'),
      'custom settings' => FALSE,
    ),
    'alternative' => array(
      'label' => t('Alternative'),
      'custom settings' => FALSE,
    ),
  );

  // Adds view modes for the taxonomy term entities
  $entity_info['taxonomy_term']['view modes'] += array(
    'teaser' => array(
      'label' => t('Teaser'),
      'custom settings' => FALSE,
    ),
    'alternative' => array(
      'label' => t('Alternative'),
      'custom settings' => FALSE,
    ),
  );
}


/**
 * Implements hook_field_extra_fields().
 */
function framastuff_field_extra_fields() {
  $fields = array();

  $fields['taxonomy_term'] = array(
    'annuaires' => array(
      'display' => array(
        'children' => array(
          'label' => t('Children list'),
          'description' => t('List of the current term\'s children (direct children).'),
          'weight' => 100,
        ),
      ),
      'form' => array(
        'os_filter' => array(
          'label' => t('Enable the OS filter'),
          'weight' => 100,
        ),
      ),
    ),
  );

  $fields['user'] = array(
    'user' => array(
      'display' => array(
        'member_for' => array(
          'label' => t('Member for'),
          'description' => t('Duration since the user is registered on the site'),
          'weight' => 100,
        ),
        'owned_sheets' => array(
          'label' => t('Owned sheets'),
          'description' => t('Sheets owned by the user'),
          'weight' => 100,
        ),
        'bookmarked_sheets' => array(
          'label' => t('Bookmarked sheets'),
          'description' => t('Sheets bookmarked by the user'),
          'weight' => 100,
        ),
      ),
    ),
  );

  return $fields;
}


/**
 * Implements hook_taxonomy_term_view().
 */
function framastuff_taxonomy_term_view($term, $view_mode, $langcode) {
  if ($term->vocabulary_machine_name == 'annuaires') {
    $ef_display = field_extra_fields_get_display('taxonomy_term', 'annuaires', $view_mode);

    if (isset($ef_display['children']) && $ef_display['children']['visible']) {
      $children = taxonomy_get_tree($term->vid, $term->tid, 1);

      if (module_exists('i18n_taxonomy')) {
        $children = i18n_taxonomy_localize_terms($children);
      }

      $links = array();

      foreach ($children as $child) {
        $links[] = array(
          'title' => $child->name,
          'href' => 'taxonomy/term/' . $child->tid,
        );
      }

      $term->content['children'] = array(
        '#theme' => 'links',
        '#links' => $links,
        '#attributes' => array('class' => array('extra-field-children')),
        '#list_group' => TRUE,
      );
    }
  }
}


/**
 * Implements hook_user_view().
 */
function framastuff_user_view($account) {
  $content_types = _framastuff_get_sheet_content_types();

  if ($sheets = NodeLoader::getOwnedBy($content_types, $account, 3)) {
    $account->content['owned_sheets'] = node_view_multiple($sheets, 'simple_teaser');
  }

  if ($bookmarks = NodeLoader::getBookmarkedBy($content_types, $account, 3)) {
    $account->content['bookmarked_sheets'] = node_view_multiple($bookmarks, 'simple_teaser');
  }

//  if ($sheets = NodeLoader::getOwnedBy($content_types, $account, 4)) {
//    $account->content['owned_sheets'] = array(
//      '#theme' => 'nodes_list',
//      '#nodes' => $sheets,
//      '#view_mode' => 'teaser',
//      '#row_size' => 2,
//    );
//  }
//
//  if ($bookmarks = NodeLoader::getBookmarkedBy($content_types, $account, 4)) {
//    $account->content['bookmarked_sheets'] = array(
//      '#theme' => 'nodes_list',
//      '#nodes' => $bookmarks,
//      '#view_mode' => 'teaser',
//      '#row_size' => 2,
//    );
//  }
}


/**
 * Implements hook_user_view_alter().
 */
function framastuff_user_view_alter(&$build) {
  $build['member_for'] = $build['summary']['member_for'];
  unset($build['member_for']['#type']);
  unset($build['member_for']['#title']);
}


//----------------------------------------------------------------------
//-- FORMS
//----------------------------------------------------------------------


/**
 * Search form for the alternatives page.
 */
function framastuff_alternatives_search_form($form, &$form_state) {
  $form['#method'] = 'GET';
  $form['#pre_render'][] = 'framastuff_method_get_form_prepare';

  if (!variable_get('clean_url', FALSE)) {
    $form['q'] = array(
      '#type' => 'hidden',
      '#value' => current_path(),
    );
  }

  $form['software'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the name of a proprietary software'),
    '#default_value' => !empty($_GET['software']) ? $_GET['software'] : '',
    '#autocomplete_path' => 'proprietary-softwares',
    '#description' => t("Please select a software in the suggestions list after having typed the first characters."),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#name' => '',
    '#theme' => 'button',
    '#theme_wrappers' => array('form_element'),
  );

  return $form;
}


/**
 * Filters form for taxonomy term pages.
 */
function framastuff_taxonomy_term_filters_form($form, &$form_state, $term) {
  $terms_with_os_filter = variable_get('terms_with_os_filter', array());

  if (in_array($term->tid, $terms_with_os_filter)) {
    $os_voc = taxonomy_vocabulary_machine_name_load('os');
    $os_terms = taxonomy_get_tree($os_voc->vid, 0, 1, TRUE);

    if (module_exists('i18n_taxonomy')) {
      $os_terms = i18n_taxonomy_localize_terms($os_terms);
    }

    $options = array('all' => t('All'));
    foreach ($os_terms as $os) {
      $options[$os->tid] = $os->name;
    }

    $form['os'] = array(
      '#type' => 'radios',
      '#title' => t('Filter by operating system'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#default_value' => (isset($_GET['os']) && ctype_digit($_GET['os'])) ? (integer) $_GET['os'] : 'all',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );

    $form['#attached'] = array(
      'js' => array(drupal_get_path('module', 'framastuff') . '/js/directory-filters.js'),
    );
  }

  if (!empty($form)) {
    $form['#method'] = 'GET';
    $form['#pre_render'][] = 'framastuff_method_get_form_prepare';

    if (!variable_get('clean_url', FALSE)) {
      $form['q'] = array(
        '#type' => 'hidden',
        '#value' => current_path(),
      );
    }

    return $form;
  }

  return array();
}


/**
 * Prepares GET method forms.
 */
function framastuff_method_get_form_prepare($form) {
  unset($form['form_token']);
  unset($form['form_build_id']);
  unset($form['form_id']);

  if (isset($form['submit'])) {
    unset($form['submit']['#name']);
  }

  return $form;
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function framastuff_form_taxonomy_form_term_alter(&$form, &$form_state, $form_id) {
  // Adds a checkbox to enable the OS filter in the software categories pages
  if (empty($form_state['confirm_delete']) && $form['#vocabulary']->machine_name == 'annuaires') {
    $terms_with_os_filter = variable_get('terms_with_os_filter', array());

    $form['os_filter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable the OS filter'),
      '#default_value' => (isset($form_state['term']->tid) && in_array($form_state['term']->tid, $terms_with_os_filter)),
    );

    $form['#submit'][] = 'framastuff_taxonomy_term_form_submit';
  }
}


/**
 * Submit callback for the taxonomy term edition form.
 */
function framastuff_taxonomy_term_form_submit(&$form, &$form_state) {
  $terms_with_os_filter = variable_get('terms_with_os_filter', array());

  $term = $form_state['term'];

  if ($form_state['values']['os_filter'] && !in_array($term->tid, $terms_with_os_filter)) {
    $terms_with_os_filter[] = $term->tid;
    variable_set('terms_with_os_filter', $terms_with_os_filter);
  }
  elseif (!$form_state['values']['os_filter'] && in_array($term->tid, $terms_with_os_filter)) {
    $key = array_search($term->tid, $terms_with_os_filter);
    unset($terms_with_os_filter[$key]);
    variable_set('$terms_with_os_filter', $terms_with_os_filter);
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function framastuff_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  $view = $form_state['view'];

  if ($view->name == 'recherche_par_criteres' && !empty($view->block_mode)) {
    $form_state['no_cache'] = TRUE;

    foreach (element_children($form) as $child_key) {
      if ($child_key != 'keys' && $child_key != 'q') {
        $form[$child_key]['#access'] = FALSE;
      }
    }
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function framastuff_form_user_login_block_alter(&$form, &$form_state, $form_id) {
  // Alters the user login form to add an explanation about why create an account
  // (Help texts are from the "contribute" block configuration)
  $texts = variable_get('framastuff_block_contribute', array(
    'help_title' => '',
    'help_text'  => array('value' => '', 'format' => 'filtered_html'),
  ));

  $form['actions']['why'] = array(
    '#theme' => 'html_tag',
    '#tag'   => 'a',
    '#value' => '<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>',
    '#attributes' => array(
      'title' => check_plain($texts['help_title']),
      'role' => 'button',
      'tabindex' => 0,
      'class' => array('text-info'),
      'data-toggle' => 'popover',
      'data-trigger' => 'focus',
      'data-placement' => 'left',
      'data-html' => 'true',
      'data-content' => check_markup($texts['help_text']['value'], $texts['help_text']['format']),
    ),
  );
}


//----------------------------------------------------------------------
//-- THEMES
//----------------------------------------------------------------------


/**
 * Implements hook_theme().
 */
function framastuff_theme($existing, $type, $theme, $path) {
  $themes = array();

  $themes['nodes_list'] = array(
    'variables' => array(
      'nodes' => array(),
      'view_mode' => 'teaser',
      'row_size' => 3,
      'block' => NULL,
    ),
    'template' => 'nodes-list',
    'path' => $path . '/templates',
  );

  $themes['page_alternatives'] = array(
    'variables' => array(
      'search_form' => array(),
      'alternatives' => array(),
    ),
    'template' => 'page-alternatives',
    'path' => $path . '/templates',
  );

  return $themes;
}


//----------------------------------------------------------------------
//-- INIT
//----------------------------------------------------------------------


/**
 * Implements hook_init().
 * Alters the path considered to render breadcrumb and menu trees.
 */
function framastuff_init() {
  $parent_path = NULL;

  // Defines the new parent path if necessary
  $item = menu_get_item();

  if ($item['path'] == 'node/%') {
    $node = menu_get_object();

    if (in_array($node->type, _framastuff_get_sheet_content_types())) {
      $category = field_get_items('node', $node, 'field_annuaires');
      $parent_path = 'taxonomy/term/' . $category[0]['tid'];
    }
  }

  // If new parent path there is, we set it for the menu tree trail future build
  if ($parent_path) {
    $preferred = menu_link_get_preferred($parent_path);
    menu_tree_set_path($preferred['menu_name'], $parent_path);

    // Fools drupal so that it takes the correct link to build the breadcrumb
    $preferred_links = &drupal_static('menu_link_get_preferred');
    $preferred_links[$_GET['q']] = $preferred_links[$parent_path];
  }
}


//----------------------------------------------------------------------
//-- MISC
//----------------------------------------------------------------------


/**
 * Implements hook_taxonomy_display_plugins().
 */
function framastuff_taxonomy_display_plugins() {
  return array(
    'associated' => array(
      'FramastuffTermAssociatedContentDisplayHandler' => t('Framasoft'),
    ),
  );
}


//----------------------------------------------------------------------
//-- HELPERS
//----------------------------------------------------------------------


/**
 * Provides content types names of those used for "sheets".
 * @return array
 */
function _framastuff_get_sheet_content_types() {
  return array(
    'logiciel_annuaires',
    'materiel',
    'livre',
    'media',
    'ressource_non_specif',
    'art_revueblogchapitrejournal',
  );
}


/**
 * Provides the render array of a block.
 */
function _framastuff_get_block_view($module, $delta) {
  $block = block_load($module, $delta);
  $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));

  if (isset($render_array[$module . '_' . $delta])) {
    return $render_array[$module . '_' . $delta];
  }

  return NULL;
}


function _framastuff_get_contribute_button_view() {
  $node_types = node_type_get_types();
  $sheet_types = _framastuff_get_sheet_content_types();
  $links = array();

  foreach ($sheet_types as $type) {
    if (!$node_types[$type]->disabled) {
      $links[] = array(
        'title' => $node_types[$type]->name,
        'href' => 'node/add/' . str_replace('_', '-', $type),
        //'attributes' => array('title' => $node_types[$type]->description),
      );
    }
  }

  return array(
    '#theme' => 'button_dropdown',
    '#label' => t('Add a sheet'),
    '#links' => $links,
    '#type' => 'primary',
    '#vertical_align' => 'right',
    '#prefix' => '<div class="btn-wrapper">',
    '#suffix' => '</div>',
  );
}


/**
 * Gets the relations alterative free software/proprietary software,
 * ordered by categories and proprietary softwares.
 * @param string $software Name of a proprietary software (see alternatives_proprios vocabulary)
 * @param integer $number Number of alternatives per page
 * @return array Rows composed of:
 *  - category ID (tid),
 *  - proprietary software ID (tid)
 *  - free software ID (nid)
 */
function _framastuff_get_alternatives($software = NULL, $number = 30) {

  $query = db_select('field_data_field_alternative_pour', 'alt');
  $query->join('node', 'soft_free', "soft_free.nid = alt.entity_id");
  $query->join('taxonomy_term_data', 'soft_prop', "soft_prop.tid = alt.field_alternative_pour_tid");
  $query->join('field_data_field_annuaires', 'cat_rel', "cat_rel.entity_id = alt.entity_id AND cat_rel.entity_type = 'node'");
  $query->join('taxonomy_term_data', 'cat', "cat.tid = cat_rel.field_annuaires_tid");
  $query->addField('alt', 'entity_id', 'free_software_nid');
  $query->addField('alt', 'field_alternative_pour_tid', 'prop_software_tid');
  $query->addField('soft_prop', 'name');
  $query->addField('cat', 'tid', 'category_tid');
  $query->addField('cat', 'weight');
  $query->condition('alt.entity_type', 'node');
  $query->condition('alt.bundle', 'logiciel_annuaires');
  $query->condition('soft_free.status', 1);
  $query->condition('alt.deleted', 0);
  $query->condition('cat_rel.deleted', 0);
  $query->orderBy('cat.tid');
  $query->orderBy('cat.weight');
  $query->orderBy('soft_prop.name');
  $query->addTag('node_access');
  $query->distinct();

  if ($software != NULL) {
    $query->condition('soft_prop.name', $software);
  }

  if ($number) {
    $query = $query->extend('PagerDefault');
    $query->limit($number);
  }

  return $query->execute()->fetchAll(PDO::FETCH_ASSOC);
}
