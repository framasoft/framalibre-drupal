<?php
/**
 * @file
 * Framastuff plugin to display terms associated content.
 */

class FramastuffTermAssociatedContentDisplayHandler extends TaxonomyDisplayAssociatedDisplayHandler
{
  /**
   * Builds the term's associated content view.
   * @see TaxonomyDisplayAssociatedDisplayHandler::displayAssociated()
   */
  public function displayAssociated($term, $options = NULL) {
    $build = array();

    // Most popular block
    $build['most_popular'] = _framastuff_get_block_view('framastuff', 'most_popular');
    $build['most_popular']['#weight'] = 10;

    // Nodes list
    $content_types = _framastuff_get_sheet_content_types();
    $filters = array();

    if (isset($_GET['os']) && ctype_digit($_GET['os'])) {
      $filters[] = array(
        'field' => 'field_os',
        'column' => 'tid',
        'value' => $_GET['os'],
      );
    }

    $vocabulary = taxonomy_vocabulary_load($term->vid);

    if ($vocabulary->machine_name === 'annuaires') {
      $nodes = NodeLoader::getAssociatedWithCategory($content_types, $term, 12, $filters);
    } else {
      $nodes = NodeLoader::getAssociatedWithTerm($content_types, $term, 12);
    }

    if (!empty($nodes)) {
      $build['filters'] = drupal_get_form('framastuff_taxonomy_term_filters_form', $term);
      $build['filters']['#weight'] = 20;

      $build['pager_top'] = array(
        '#theme' => 'pager',
        '#weight' => 30,
      );

      $build['nodes'] = array(
        '#theme' => 'nodes_list',
        '#nodes' => $nodes,
        '#view_mode' => 'teaser',
        '#weight' => 40,
      );

      $build['pager_bottom'] = array(
        '#theme' => 'pager',
        '#weight' => 50,
      );
    }
    else {
      // If a filter has been received, then we display the filters form
      if (isset($_GET['os']) && ctype_digit($_GET['os'])) {
        $build['filters'] = drupal_get_form('framastuff_taxonomy_term_filters_form', $term);
        $build['filters']['#weight'] = 20;
      }

      $build['no_content'] = array(
        '#markup' => t('There is currently no sheet classified with this term.'),
        '#prefix' => '<p class="no-content alert alert-info">',
        '#suffix' => '</p>',
        '#weight' => 40,
      );
    }

    return $build;
  }

  /**
   * Provides the configuration form of the plugin.
   * @see TaxonomyDisplayAssociatedDisplayHandler::formFieldset()
   */
  public function formFieldset(&$form, &$values, $options = NULL) {}

  /**
   * Prepares the configuration values before storage.
   * @see TaxonomyDisplayAssociatedDisplayHandler::formSubmit()
   */
  public function formSubmit($form, &$values) {}
}